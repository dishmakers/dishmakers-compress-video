<?php

class VC_Compress_Video_Settings {

    public function __construct() {
    	// Hook into the admin menu
    	add_action( 'admin_menu', array( $this, 'compress_video_settings_page' ) );

        // Add Settings and Fields
    	add_action( 'admin_init', array( $this, 'setup_sections' ) );
    	add_action( 'admin_init', array( $this, 'setup_fields' ) );
    }


    public function compress_video_settings_page() {
    	// Add the menu item and page
    	$page_title = 'Compress Video';
    	$menu_title = 'Compress Video';
    	$capability = 'manage_options';
    	$slug = 'compress_video_settings';
    	$callback = array( $this, 'plugin_settings_page_content' );
    	$icon = 'dashicons-admin-plugins';
    	$position = 100;

    	add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
    }

    public function plugin_settings_page_content() {?>
    	<div class="wrap">
    		<h2>Compress Video Settings</h2><?php
            if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] ){
                  $this->admin_notice();
            } ?>
    		<form method="POST" action="options.php">
                <?php
                    settings_fields( 'compress_video_fields' );
                    do_settings_sections( 'compress_video_fields' );
                    submit_button();
                ?>
    		</form>
    	</div> <?php
    }
    
    public function admin_notice() { ?>
        <div class="notice notice-success is-dismissible">
            <p>Your settings have been updated!</p>
        </div><?php
    }

    public function setup_sections() {
        add_settings_section( 'cron_section', 'Cron settings', array( $this, 'section_callback' ), 'compress_video_fields' );
        add_settings_section( 'video_section', 'Video settings', array( $this, 'section_callback' ), 'compress_video_fields' );
    }

    public function section_callback( $arguments ) {
    	switch( $arguments['id'] ){
    		case 'cron_section':
    			//echo 'Cron settings';
    			break;
            case 'video_section':
                //echo 'Cron settings';
                break;
    	}
    }

    public function setup_fields() {
        $fields = array(
            array(
                'uid' => 'vc_interval_time_field',
                'label' => 'Interval Time',
                'section' => 'cron_section',
                'type' => 'text',
                'value' => '60',
                'placeholder' => '60',
                'supplimental' => 'Cron Interval Time per seconde',
            ),
            array(
                'uid' => 'vc_server_url',
                'label' => 'Server Url',
                'section' => 'video_section',
                'type' => 'text',
                'value' => 'http://127.0.0.1:9025',
                'placeholder' => 'http://127.0.0.1:9025'
            ),
            array(
                'uid' => 'vc_maxrate',
                'label' => 'Maxrate',
                'section' => 'video_section',
                'type' => 'text',
                'value' => '500k',
                'placeholder' => '500k'
            )

        );
    	foreach( $fields as $field ){

        	add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'compress_video_fields', $field['section'], $field );
            register_setting( 'compress_video_fields', $field['uid'] );
    	}
    }

    public function field_callback( $arguments ) {

        $value = get_option( $arguments['uid'] );

        if( ! $value ) {
            $value = isset($arguments['default']) ? $arguments['default'] : '';
        }

        switch( $arguments['type'] ){
            case 'text':
            case 'password':
            case 'number':
                printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
                break;
            case 'textarea':
                printf( '<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>', $arguments['uid'], $arguments['placeholder'], $value );
                break;
            case 'select':
            case 'multiselect':
                if( ! empty ( $arguments['options'] ) && is_array( $arguments['options'] ) ){
                    $attributes = '';
                    $options_markup = '';
                    foreach( $arguments['options'] as $key => $label ){
                        $options_markup .= sprintf( '<option value="%s" %s>%s</option>', $key, selected( $value[ array_search( $key, $value, true ) ], $key, false ), $label );
                    }
                    if( $arguments['type'] === 'multiselect' ){
                        $attributes = ' multiple="multiple" ';
                    }
                    printf( '<select name="%1$s[]" id="%1$s" %2$s>%3$s</select>', $arguments['uid'], $attributes, $options_markup );
                }
                break;
            case 'radio':
            case 'checkbox':
                if( ! empty ( $arguments['options'] ) && is_array( $arguments['options'] ) ){
                    $options_markup = '';
                    $iterator = 0;
                    foreach( $arguments['options'] as $key => $label ){
                        $iterator++;
                        $options_markup .= sprintf( '<label for="%1$s_%6$s"><input id="%1$s_%6$s" name="%1$s[]" type="%2$s" value="%3$s" %4$s /> %5$s</label><br/>', $arguments['uid'], $arguments['type'], $key, checked( $value[ array_search( $key, $value, true ) ], $key, false ), $label, $iterator );
                    }
                    printf( '<fieldset>%s</fieldset>', $options_markup );
                }
                break;
        }

        if( $helper = isset($arguments['helper']) ? $arguments['helper'] : false ){
            printf( '<span class="helper"> %s</span>', $helper );
        }

        if( $supplimental = isset($arguments['supplimental']) ? $arguments['supplimental'] : false ){
            printf( '<p class="description">%s</p>', $supplimental );
        }

    }

}
$vc_compress_video_settings = new VC_Compress_Video_Settings();