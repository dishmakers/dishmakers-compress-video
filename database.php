<?php
class CV_database_plugin_db {
	public $version='1';
	public $table_name = 'videos_compress';
	public function __construct() {
		
	}
	public function create_plugin_database_table(){

		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	    global $wpdb;
	    $sql_request = "CREATE TABLE IF NOT EXISTS `". $wpdb->prefix.$this->table_name."` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `attachment_id` int(11) NOT NULL,
		  `scale` varchar(150) NOT NULL,
		  `status` varchar(150) NOT NULL,
		  `file_name` varchar(150) NOT NULL DEFAULT '',
		  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  CONSTRAINT video_info UNIQUE(`attachment_id`,`scale`),
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
	    dbDelta($sql_request);
	}
	public function get_videos($status = 'compressed',$number=10){
		global $wpdb;
		$results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}".$this->table_name." WHERE status=%s ORDER BY date ASC LIMIT 0,%d", $status,$number) );
		return $results;
	}

	public function get_videos_by_id($id = 'compressed'){
		global $wpdb;
		$results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}".$this->table_name." WHERE attachment_id=%s ORDER BY date ASC", $id) );
		return $results;
	}

	public function check_videos($status='compressed'){
		global $wpdb;
		$rowcount = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(id) as total FROM {$wpdb->prefix}".$this->table_name." WHERE status=%s", $status) );
		return ( $rowcount == 0) ? false : true ;
	}

	public function get_video( $attachment=null , $scale = null){
		global $wpdb;
		if( $attachment ){
			if( $scale )
				$results = $wpdb->get_row( $wpdb->prepare("SELECT * FROM {$wpdb->prefix}".$this->table_name." WHERE attachment_id=%d AND scale=%d", $attachment, $scale) );
			else
				$results = $wpdb->get_row( $wpdb->prepare("SELECT * FROM {$wpdb->prefix}".$this->table_name." WHERE attachment_id=%d", $attachment) );
			return $results;
		}
		return false;
	}

	public function set_video($data=array()){
		global $wpdb;
		if( isset($data['attachment_id']) && !empty($data['attachment_id']) && !empty($data['scale']) ){
			if( !$this->get_video($data['attachment_id'] , $data['scale']) ){
				$data_insert = array();
				
				try {
					$data_insert = array(
					'attachment_id' => $data['attachment_id'],
					'scale' => $data['scale'],
					'status' => (isset($data['status'])) ? $data['status'] : 'pending'
					);

					$format = array('%d','%s','%s');
					$wpdb->insert($wpdb->prefix.$this->table_name,$data_insert,$format);
				} catch (Exception $e) {
				}
				$my_id = $wpdb->insert_id;
				return $my_id; 
			}
		}
		return false;
	}

	public function update_video($attachment=null, $scale=null, $data=array()){
		global $wpdb;
		if( $attachment && !empty($attachment) ){
			$where = [ 'attachment_id' => $attachment,'scale' => $scale ]; // NULL value in WHERE clause.
			$wpdb->update( $wpdb->prefix.$this->table_name, $data, $where ); // Also works in this case.
			return true;
		}
		return false;
	}

	public function delete_video($id=null, $scale='2480'){
		global $wpdb;
		if( $id && !empty($id) && !empty($scale) ){
			$wpdb->delete( $wpdb->prefix.$this->table_name, array( 'attachment_id' => $id, 'scale' => $scale ) );
			delete_post_meta($id, '_vc_compressed_video_url_'.$scale);
			return true;
		}
		return false;
	}
}
