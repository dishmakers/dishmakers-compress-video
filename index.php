<?php
/*

Plugin Name: Compress Video Plugin

Plugin URI: http://yasminawebagency.com/

Description: Compress uploaded video

Author: Sabri Bouchaala

Version: 0.1s

Author URI: http://www.yasminawebagency.com/

*/

/* Remove query strings from scripts and stylesheets */
require_once(trailingslashit( plugin_dir_path( __FILE__ ) ) .'/database.php');
register_activation_hook( __FILE__, 'cv_plugin_database_table' );
function cv_plugin_database_table() {
    $cv_database_plugin_db = new CV_database_plugin_db();
    $cv_database_plugin_db->create_plugin_database_table();
}
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ).'/admin/page-options.php' );
class Compress_Video {

    public $cv_database_plugin_db;
    public $scales = array('2480'=>'-2:480','2720'=>'-2:720','21080'=>'2:1080');
    public $vc_maxrate;
	function __construct() {
        $this->cv_database_plugin_db = new CV_database_plugin_db();
        $vc_maxrate = get_option('vc_maxrate');
        $this->vc_maxrate = isset( $vc_maxrate ) && !empty($vc_maxrate) ? $vc_maxrate : '500k';
	 	add_action( 'init', array( $this, 'constants' ), 0 );
        add_action( 'init', array( $this, 'load_localisation' ), 0 );
        add_action ('admin_init' , array( $this , 'admin_init') );
        add_action('admin_enqueue_scripts' , array( $this, 'load_custom_wp_admin_style' ) );
        add_filter( 'attachment_fields_to_edit',  array( $this, 'add_compress_video_button'), 10, 2 );
        add_action( 'wp_ajax_compress_video', array( $this, 'ajax_compress_video' ) );
        register_activation_hook(__FILE__, array( $this, 'activation' ) );
        add_filter( 'cron_schedules', array( $this, 'custom_cron_job_recurrence' ) ) ;
        add_action('compress_video_event', array( $this, 'do_compress_video_event' ) );
        add_action( 'add_attachment', array( $this, 'add_attachment' ) );
        add_action( 'delete_attachment', array( $this, 'delete_attachment' ) );
	}
    public function admin_init(){}
    public function delete_attachment( $post_ID ) {
        foreach ($this->scales as $key => $scale) {
            $video = $this->cv_database_plugin_db->get_video( $post_ID, $key );
            if( $video ){
                if( !empty( $video->file_name ) ){
                    $upload_dir   = wp_upload_dir();
                    $file_path = $upload_dir['basedir'].'/compress_videos/'.$video->file_name;
                    wp_delete_file( $file_path );
                }
                $this->cv_database_plugin_db->delete_video( $post_ID , $key );    
            }
        }
    }
    public function add_attachment( $post_ID ) { 
        if( $post_ID  && wp_attachment_is( 'video', $post_ID ) ) {
            $generate=false;
            $data_insert = array();
            if( !$this->cv_database_plugin_db->check_videos( 'processing' ) ){
                if( !$this->cv_database_plugin_db->check_videos( 'pending' ) ){
                    foreach ($this->scales as $key => $scale) {
                        $data_insert = array( 
                            'attachment_id' => $post_ID,
                            'scale' => $key,
                            'status' => 'pending'
                        );
                        $video_id = $this->cv_database_plugin_db->set_video( $data_insert );
                        if( $video_id ){
                            $data = array('scale' => $scale ,'maxrate' => $this->vc_maxrate );
                            $this->compress_video( $post_ID,$key, $data );
                        }
                    }

                }else
                    $generate=true;
           }else
                $generate=true;
            
            if( $generate ){
                foreach ($this->scales as $key => $scale) {
                    $data_insert = array( 
                        'attachment_id' => $post_ID,
                        'scale' => $key,
                        'status' => 'pending'
                    );
                    error_log(json_encode($data_insert));
                    $video = $this->cv_database_plugin_db->set_video( $data_insert );
                }
            }

        }

    }
    public function do_compress_video_event(){
        if( !$this->cv_database_plugin_db->check_videos( 'processing' ) ){
            if( $this->cv_database_plugin_db->check_videos( 'pending' ) ){
                $videos  = $this->cv_database_plugin_db->get_videos( 'pending',1 );
                if( $videos ){
                    foreach ( $videos as $key => $video ) {
                        if( wp_attachment_is( 'video', $video->attachment_id ) ) {
                            $data = array('scale' => $this->scales[$video->scale] ,'maxrate' => $this->vc_maxrate );
                            $this->compress_video( $video->attachment_id, $video->scale , $data );
                        }
                    }
                }
            }
        }
    }

    public function custom_cron_job_recurrence( $schedules ) {
        $vc_interval_time_field = get_option('vc_interval_time_field');
        $interval_time = isset( $vc_interval_time_field ) && !empty($vc_interval_time_field) ? $vc_interval_time_field : 60;
        if(!isset($schedules['compress_video_cron'])){
            $schedules['compress_video_cron'] = array(
            'display' => sprintf(__( 'Every %s Seconds', 'compress_video' ),$interval_time),
            'interval' => $interval_time,
            );
        }    
        return $schedules;
    }

 
    public function activation() {
        if (! wp_next_scheduled ( 'compress_video_event' )) {
        wp_schedule_event(time(), 'compress_video_cron', 'compress_video_event');
        }
    }
    public function ajax_compress_video(){
        if ( ! check_ajax_referer( 'cv-security-nonce', 'security', false ) ){
            $results = array( 
                'success' => 'false',
                'message' => "Error");
            echo json_encode($results);
           wp_die();
        }
        $results = array( 
                'success' => 'false',
                'message' => "Error"
        );
        $attachment_id = isset( $_POST['id'] ) ? $_POST['id'] : null;
        $file_url = '';
        $url = array();
        if( $attachment_id  && wp_attachment_is( 'video',$attachment_id ) ) {
            foreach ($this->scales as $key => $scale) {
                $data = array('scale' => $scale ,'maxrate' => $this->vc_maxrate );
                $data_insert = array( 
                    'attachment_id' => $attachment_id,
                    'scale' => $key,
                    'status' => 'pending'
                );
                $video = $this->cv_database_plugin_db->get_video( $attachment_id , sanitize_title($scale));

                if( $video ){
                    if( !empty( $video->file_name ) ){
                        $upload_dir   = wp_upload_dir();
                        $file_path = $upload_dir['basedir'].'/compress_videos/'.$video->file_name;
                        wp_delete_file( $file_path );
                    }
                    $this->cv_database_plugin_db->delete_video( $attachment_id, $key );    
                }

                $video_id = $this->cv_database_plugin_db->set_video( $data_insert );
                if( $video_id ){

                    $file_url = $this->compress_video( $attachment_id ,$key, $data); 
                    if( $file_url ){
                        $message = __( "Video Compressed Successfully" ,'compress_video');
                        $results = array( 
                            'success' => 'true',
                            'message' => $message,
                            
                        );
                        $url[$key] = $file_url;
                    }
                }
            }
        }
        $results['url'] = $url;
        echo json_encode($results);
        wp_die();
    }
    public function compress_video( $attachment_id=null , $scale=null , $data=array()){

        $file_path = get_attached_file( $attachment_id );
        $ch = curl_init();
        $fields = $data;
        // files to upload
        $files = array();
        $files[] = file_get_contents($file_path);
        // URL to upload to
        $vc_server_url = get_option('vc_server_url');
        $vc_server_url = isset( $vc_server_url ) && !empty( $vc_server_url ) ? $vc_server_url : 'http://172.18.0.5:3000';
        $url = $vc_server_url.'/'.COMPRESS_SERVER_EXT;
        // curl
        $curl = curl_init();
        //$url_data = http_build_query($fields);
        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;
        $post_data = self::build_data_files($boundary, $fields, $files);
        curl_setopt_array($ch, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $post_data,
          CURLOPT_HTTPHEADER => array(
            //"Authorization: Bearer $TOKEN",
            "Content-Type: multipart/form-data; boundary=" . $delimiter,
            "Content-Length: " . strlen($post_data)
          )
        ));
        $data_update = array( 
            'status' => 'processing'
        );
        $video = $this->cv_database_plugin_db->update_video( $attachment_id , $scale , $data_update );
        $response = curl_exec($ch);
        if(curl_errno($ch) == 0  && $response !== false && $video ){
            $ext = pathinfo($file_path, PATHINFO_EXTENSION);
            $without_extension = basename($file_path,".".$ext);
            $upload_dir   = wp_upload_dir();
            $dir = $upload_dir['basedir'].'/compress_videos/';
            if( !is_dir( $dir ) )
                mkdir($dir);
            file_put_contents($dir.$without_extension.'_'.sanitize_title($fields['scale']).'_compressed.'.COMPRESS_SERVER_EXT, $response, LOCK_EX);
            require_once('inc/getid3/getid3.php');
            curl_close($ch);
            if ( file_exists( $dir.$without_extension.'_'.sanitize_title($fields['scale']).'_compressed.'.COMPRESS_SERVER_EXT ) ) {
                $engine = new getID3;
                $fileinfo = $engine->analyze($dir.$without_extension.'_'.sanitize_title($fields['scale']).'_compressed.'.COMPRESS_SERVER_EXT);
                if( $fileinfo['filesize'] <= 0 || $fileinfo['fileformat'] != 'mp4'){
                     $data_update = array( 
                        'status' => 'uncompressed'
                    );
                    $video = $this->cv_database_plugin_db->update_video( $attachment_id, $scale , $data_update );
                    return false;
                }
            }else{
                $data_update = array( 
                    'status' => 'uncompressed'
                );
                $video = $this->cv_database_plugin_db->update_video( $attachment_id, $scale, $data_update );
                return false;
            }
            $file_url = $upload_dir['baseurl'].'/compress_videos/'.$without_extension.'_'.sanitize_title($fields['scale']).'_compressed.'.COMPRESS_SERVER_EXT;
            $data_update = array( 
                'status' => 'compressed',
                'file_name' => $without_extension.'_'.sanitize_title($fields['scale']).'_compressed.'.COMPRESS_SERVER_EXT
            );
            
            $video = $this->cv_database_plugin_db->update_video( $attachment_id, $scale, $data_update );
            update_post_meta( $attachment_id, '_vc_compressed_video_url_'.sanitize_title($fields['scale']), $file_url );
            return $file_url;
        }
        curl_close($ch);
        return false;
    }
    static function check_format_file(){

    }
    static function build_data_files($boundary, $fields, $files){
        $data = '';
        $eol = "\r\n";
        $delimiter = '-------------' . $boundary;
        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }

        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary'.$eol
                ;

            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--".$eol;
        return $data;
    }

    /*--------------------------------------------*

    * Localisation | Public | 1.0 | Return : void

    *--------------------------------------------*/

    public function load_localisation ()

    {
        global $layers_customizer_controls;
        load_plugin_textdomain( 'compress_video', false, basename( dirname( __FILE__ ) ) . '/languages' );
    } // End load_localisation()

    /**
    * Defines constants for the plugin.
    */
    public function constants() {
        define( 'COMPRESS_VIDEO_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
        define( 'COMPRESS_VIDEO_URL', plugins_url('compress-video') );
        define( 'COMPRESS_SERVER_EXT', 'mp4');
    }

    public function load_custom_wp_admin_style() {
        wp_enqueue_script( 'cv_custom_script', COMPRESS_VIDEO_URL . '/assets/js/admin-script.js' , array( 'jquery','media-upload' ), '0.0.2', true);
        
               // pass Ajax Url to script.js
        wp_localize_script('cv_custom_script', 'cv_ajaxurl', array(
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('cv-security-nonce')
        ) );
        wp_register_style( 'cv_wp_admin_css', COMPRESS_VIDEO_URL . '/assets/css/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'cv_wp_admin_css' );

    }

    public function add_compress_video_button( $form_fields, $post ) {
        if( wp_attachment_is( 'video', $post->ID ) ) {
            $videos = $this->cv_database_plugin_db->get_videos_by_id( $post->ID );
            $text = 'Compress';
            $class = "";
            $file_url = '';
            $upload_dir   = wp_upload_dir();
 
            $data = array();
            foreach ($videos as $key => $video) {
                if( $video && $video->status == 'compressed'){
                        $text = 'Compressed';
                        $class = " compressed";
                    if( !empty( $video->file_name ) ){
                        $data[$video->scale]['file_url'] = $upload_dir['baseurl'].'/compress_videos/'.$video->file_name;
                    }
                }
            }

            $form_fields['compress'] = array(
                'label' => '',
                'input' => 'html',
                "html" => "<script>
                jQuery(document).ready(function($){
                    jQuery('button.compress_video_btn').on('click',function(){
                        var button = $(this);
                        var id = button.data('id');
                        button.addClass('progress');
                        button.find('.text').text('Compression');
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            url: cv_ajaxurl.url,
                            data: {
                                id: button.data('id'),
                                action:'compress_video',
                                security:cv_ajaxurl.nonce
                            },
                            success: function(data){

                                button.removeClass('progress');
                                button.next('.message').show();
                                if( data.success == 'true'){
                                    button.find('.text').text('Compressed');
                                    button.addClass('compressed');
                                    $.each(data.url,function(k,val){
                                        $('.compat-field-compress_url_file_'+k+' .field input').val(val);
                                    });
                                    button.next('.message').addClass('success');
                                    button.next('.message').text(data.message);
                                }
                                else{
                                    button.find('.text').text('Compress');
                                    button.removeClass('compressed');
                                    button.next('.message').addClass('error');
                                    button.next('.message').text('Video not compressed');
                                }
                            }
                        });
                    });
                });
                </script>
                <button class='btn compress_video_btn".$class."' data-id='".$post->ID."'><div class='text'>".$text."</div><span class='spin'></spin></button>
                <div class='compress_video_message message notice'></div>",
                'helps' => '',
            );
            foreach ($this->scales as $key => $scale) {
                $file_url = isset($data[$key]['file_url']) ? $data[$key]['file_url'] : '';
                $form_fields['compress_url_file_'.$key] = array(
                    'label' => 'Compression url '.$scale,
                    'input' => 'html', // you may alos use 'textarea' field
                    'html' => "<input type='text' value='".$file_url."' readonly=''>",
                    'helps' => ''

                );
            }
           
        }
        return $form_fields;
    }




}// End Class

// Initiation call of plugin

$compress_video = new Compress_Video(__FILE__);